PostgreSQL - educ
-----------------

A simple docker image that contains tables with pre-loaded datasets.

The user and password are hardcoded: 
	user: `pguser`
	password: `pguser`

# How to:
## Build the image:
```
docker build -t csc014psql .
```

## Run the image
```
docker run -t -i -p 5432:5432 csc014psql
```

## Access the server
This assumes that the psql tool is installed from the client. Alternatively, you should be able to access it from anywhere.
```
psql -h localhost -p 5432 -U pguser -W pgdb
```
# Installed datasets

nycflights

# Utilities
Backup the database (can be run from outside the container)
```
pg_dump -h localhost -U pguser -W pgdb >> flights.sql
```
