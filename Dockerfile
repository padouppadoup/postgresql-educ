FROM debian
MAINTAINER Maxime Woringer <maxime@woringer.fr>

ENV PGPW='pguser'

RUN apt-get update && apt-get install -y --no-install-recommends postgresql

USER postgres
RUN service postgresql start \
  && psql --command "CREATE USER pguser WITH SUPERUSER PASSWORD 'pguser';" \
  && createdb -O pguser pgdb

USER root
RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/9.6/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/9.6/main/postgresql.conf

COPY flights.sql flights.sql
RUN service postgresql start && sleep 10 && PGPASSWORD=pguser psql -U pguser -h localhost pgdb < flights.sql

EXPOSE 5432
CMD service postgresql start;/bin/bash